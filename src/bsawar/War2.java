package bsawar;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Random;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

/**
 *
 * @author Austin
 */
public class War2 extends JFrame implements ActionListener {
    //These are Objects that fetch the card images
    private CardImages ci = new CardImages();
    private CardImages one = new CardImages();
    //WarDraw is another object that compares the value of the card
    //and increments the number of wins or performs WAR!
    private WarDraw draw = new WarDraw();
    private Boolean didDraw = false;
    //Two String arrays
    private static String[] suits = {"Hearts", "Spades", "Diamonds", "Clubs"};
    private static String[] ranks = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
//    Buttons for the plkayers
    private JButton playerOneButton = new JButton("Draw!");
    private JButton playerTwoButton = new JButton("Draw!");
    
    private ImageIcon image = image1("WarLogo.jpg");
    private JLabel warLogo = new JLabel (image);
    
    private JTextArea textPane = new JTextArea();
    
    
    private ImageIcon image1(String thePath) {
        URL theURL = getClass().getResource(thePath);
        if (theURL != null) {
            return new ImageIcon(theURL);
        } else {
            System.err.println("Couldn't find file: " + thePath);
            return null;
        }
    }
    
    //Label to hold the images
    private JLabel playerOneImage = new JLabel();
    //Labels that determine the player and their points
    private JLabel playerOneOutput = new JLabel("PlayerOne");
    private JLabel playerTwoOutput = new JLabel("PlayerTwo");
    //Two random number generators for each player
    private Random rankWar = new Random();
    private Random twoWar = new Random();
    //A integer range for each Random generator
    private int randomRank = 51;
    private int randomR = 51;
    //Intergers that will store the randomly generated number
     int rNum;
     int rW;
     int total = 0;
    private int onePoints = 0;
    private int twoPoints = 0;
    //Interger for do while loop
    private int i = 0;
    //Number of turns each player has
    private int oneTurns = 52;
    private int twoTurns = 52;
    Scanner scan = new Scanner(System.in);
        
    //War constructos
    public War2()

        {
        super("War");
        oneTurns = 26;
        twoTurns = 26;
        System.out.println("Welcome to War(The card game-Duh)");
        playerOneButton.addActionListener(this);
        playerTwoButton.addActionListener(this);
        playerTwoButton.setVisible(false);
        playerOneButton.setVisible(true);
     
       
        
   
    }
    //Method performs a tripple draw for a War sceanario
      private void doDraw(){
          didDraw = false;
      do{
        rNum = rankWar.nextInt((randomRank) - 26);
        rW = rankWar.nextInt((randomR) + 26);
        i++;
      }while(i < 3);
        didDraw = true;
        draw.setDraw(didDraw);
        
        
    }
   
    //Two Containers to hold each player
//      and then put into one Container
    public Container warPanel() {
        JPanel north = new JPanel();
        north.add(this.warLogo);
        
        JPanel west = new JPanel();
            west.add(playerOneOutput);
            
            west.add(playerOneButton);
            west.add(one);
        JPanel east = new JPanel();
            east.add(playerTwoButton);
            east.add(playerTwoOutput);
            east.add(ci);
        
        
        JPanel southWest = new JPanel(new FlowLayout());
            
            JScrollPane scrollPane = new JScrollPane(this.textPane);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            scrollPane.setPreferredSize(new Dimension(400, 200));
            textPane.setEditable(false);
            southWest.add(scrollPane);
        
        Container warContainer = getContentPane();
        warContainer.setLayout(new BorderLayout(100, 100));
        warContainer.add(north, BorderLayout.NORTH);
        warContainer.add(west, BorderLayout.WEST);
        warContainer.add(east, BorderLayout.EAST);
        warContainer.add(southWest, BorderLayout.SOUTH);
        return warContainer;

    }
      public void useWarDraw(){   
          
          total ++;
            try {
                rNum = twoWar.nextInt(randomR);
                System.out.print(randomRank);
                draw.setPlayer(rW, rNum, oneTurns, twoTurns, onePoints, twoPoints);
            } catch (IllegalArgumentException exc) {
                System.err.println(exc);
                
            }
            oneTurns = draw.getOneTurn();
            twoTurns = draw.getTwoTurn();   
           
            onePoints = draw.getOnePoints();
             System.out.println(onePoints);
            twoPoints = draw.getTwoPoints();
            String sonePoints = String.valueOf(onePoints);
            String stwoPoints = String.valueOf(twoPoints);
           //Retrieves points and sets the text of the TextPane
            //The Text Area is used to append all of the information
            //about each round
            textPane.append("\n--------------------\nTotal rounds played: " + total + "\n");
            textPane.append("\n--------------------\nPlayer One's Points:\n" + sonePoints);
            textPane.append("\nPlayer One's Cards:\n" + String.valueOf(draw.getOneTurn()) + "\n");
            
            textPane.append("\n--------------------\nPlayer Two's Points:\n" + stwoPoints);
            textPane.append("\nPlayer Two's Cards:\n" + String.valueOf(draw.getTwoTurn()) + "\n");
           
         }
    public void mySwitch(){
        /*
         *Each case is matched by the Random number that is
         * generated each time.  The case will then append the cards
         * Rank and Suit.  Also the mathching image.
         
         */
     switch (rW) {

                case 0:
                    textPane.append("\n" + ranks[0] + " of " + suits[0]);
                    one.getAceHearts();
                    break;
                case 1:
                    textPane.append("\n" + ranks[0] + " of " + suits[1]);
                    one.getAceSpades();
                    break;
                case 2:
                    textPane.append("\n" + ranks[0] + " of " + suits[2]);
                    one.getAceDia();

                    break;
                case 3:
                    textPane.append("\n" + ranks[0] + " of " + suits[3]);
                    one.getAceClubs();

                    break;
                case 4:
                    textPane.append("\n" + ranks[1] + " of " + suits[0]);
                    one.getTwoHearts();

                    break;
                case 5:
                    textPane.append("\n" + ranks[1] + " of " + suits[1]);
                    ci.getTwoSpades();

                    break;
                case 6:
                    textPane.append("\n" + ranks[1] + " of " + suits[2]);
                    one.getTwoDia();

                    break;
                case 7:
                    textPane.append("\n" + ranks[1] + " of " + suits[3]);
                    one.getTwoClubs();
                    break;
                case 8:
                    textPane.append("\n" + ranks[2] + " of " + suits[0]);
                    one.getThreeHearts();

                    break;
                case 9:
                    textPane.append("\n" + ranks[2] + " of " + suits[1]);
                    one.getThreeSpades();

                    break;
                case 10:
                    textPane.append("\n" + ranks[2] + " of " + suits[2]);
                    one.getThreeDia();

                    break;
                case 11:
                    textPane.append("\n" + ranks[2] + " of " + suits[3]);
                    one.getThreeClubs();

                    break;
                case 12:
                    textPane.append("\n" + ranks[3] + " of " + suits[0]);
                    one.getFourHearts();

                    break;
                case 13:
                    textPane.append("\n" + ranks[3] + " of " + suits[1]);
                    one.getFourSpades();

                    break;

                case 14:
                    textPane.append("\n" + ranks[3] + " of " + suits[2]);
                    one.getFourDia();

                    break;
                case 15:
                    textPane.append("\n" + ranks[3] + " of " + suits[3]);
                    one.getFourClubs();

                    break;
                case 16:
                    textPane.append("\n" + ranks[4] + " of " + suits[0]);
                    one.getFiveHearts();

                    break;
                case 17:
                    textPane.append("\n" + ranks[4] + " of " + suits[1]);
                    one.getFiveSpades();

                    break;
                case 18:
                    textPane.append("\n" + ranks[4] + " of " + suits[2]);
                    one.getFiveDia();

                    break;
                case 19:
                    textPane.append("\n" + ranks[4] + " of " + suits[3]);
                    one.getFiveClubs();

                    break;
                case 20:
                    textPane.append("\n" + ranks[5] + " of " + suits[0]);
                    one.getSixHearts();

                    break;
                case 21:
                    textPane.append("\n" + ranks[5] + " of " + suits[1]);
                    one.getSixSpades();

                    break;
                case 22:
                    textPane.append("\n" + ranks[5] + " of " + suits[2]);
                    one.getSixDia();

                    break;
                case 23:
                    textPane.append("\n" + ranks[5] + " of " + suits[3]);
                    one.getSixClubs();

                    break;
                case 24:
                    textPane.append("\n" + ranks[6] + " of " + suits[0]);
                    one.getSevenHearts();

                    break;
                case 25:
                    textPane.append("\n" + ranks[6] + " of " + suits[1]);
                    one.getSevenSpades();

                    break;
                case 26:
                    textPane.append("\n" + ranks[6] + " of " + suits[2]);
                    one.getSevenDia();

                    break;
                case 27:
                    textPane.append("\n" + ranks[6] + " of " + suits[3]);
                    one.getSevenClubs();

                    break;
                case 28:
                    textPane.append("\n" + ranks[7] + " of " + suits[0]);
                    one.getEightHearts();

                    break;
                case 29:
                    textPane.append("\n" + ranks[7] + " of " + suits[1]);
                    one.getEightSpades();

                    break;
                case 30:
                    textPane.append("\n" + ranks[7] + " of " + suits[2]);
                    one.getEightDia();

                    break;
                case 31:
                    textPane.append("\n" + ranks[7] + " of " + suits[3]);
                    one.getEightClubs();

                    break;
                case 32:
                    textPane.append("\n" + ranks[8] + " of " + suits[0]);
                    one.getNineHearts();

                    break;
                case 33:
                    textPane.append("\n" + ranks[8] + " of " + suits[1]);
                    one.getNineSpades();

                    break;
                case 34:
                    textPane.append("\n" + ranks[8] + " of " + suits[2]);
                    one.getNineDia();

                    break;
                case 35:
                    textPane.append("\n" + ranks[8] + " of " + suits[3]);
                    one.getNineClubs();

                    break;
                case 36:
                    textPane.append("\n" + ranks[9] + " of " + suits[0]);
                    one.getTenHearts();

                    break;
                case 37:
                    textPane.append("\n" + ranks[9] + " of " + suits[1]);
                    one.getTenSpades();

                    break;
                case 38:
                    textPane.append("\n" + ranks[9] + " of " + suits[2]);
                    one.getTenDia();

                    break;
                case 39:
                    textPane.append("\n" + ranks[9] + " of " + suits[3]);
                    one.getTenClubs();

                    break;
                case 40:
                    textPane.append("\n" + ranks[10] + " of " + suits[0]);
                    one.getJackHearts();

                    break;
                case 41:
                    textPane.append("\n" + ranks[10] + " of " + suits[1]);
                    one.getJackSpades();

                    break;
                case 42:
                    textPane.append("\n" + ranks[10] + " of " + suits[2]);
                    one.getJackDia();

                    break;
                case 43:
                    textPane.append("\n" + ranks[10] + " of " + suits[3]);
                    one.getJackClubs();

                    break;
                case 44:
                    textPane.append("\n" + ranks[11] + " of " + suits[0]);
                    one.getQueenHearts();

                    break;
                case 45:
                    textPane.append("\n" + ranks[11] + " of " + suits[1]);
                    one.getQueenSpades();

                    break;
                case 46:
                    textPane.append("\n" + ranks[11] + " of " + suits[2]);
                    one.getQueenDia();

                    break;
                case 47:
                    textPane.append("\n" + ranks[11] + " of " + suits[3]);
                    one.getQueenClubs();

                    break;
                case 48:
                    textPane.append("\n" + ranks[12] + " of " + suits[0]);
                    one.getKingHearts();

                    break;
                case 49:
                    textPane.append("\n" + ranks[12] + " of " + suits[1]);
                    one.getKingSpades();

                    break;
                case 50:
                    textPane.append("\n" + ranks[12] + " of " + suits[2]);
                    one.getKingDia();

                    break;
                case 51:
                    textPane.append("\n" + ranks[12] + " of " + suits[3]);
                    one.getKingClubs();

                    break;
                default:
                    System.out.print(rW);
                    break;
            }
    }
  

    public static void main(String[] args) {
        // TODO code application logic here
        War2 m = new War2();
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        m.setContentPane(m.warPanel());
        m.setLocation(550, 250);
        m.setSize(600, 675);
        m.setVisible(true);
    }
    //ActionListener
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        /*
         * when the player Two button is clicked it will set itself to invisible
         * and ones button to visible, distinguising turns.  Also, the players turns
         * are decremented by 1.
         * 
         * 
         */
        if (o == playerTwoButton) {
           playerTwoButton.setVisible(false);
            playerOneButton.setVisible(true);
            
           /*
            * Trys to generate a random Int
            * uses the object Draw to send the numbers to the
            * WarDraw class
            * 
            */
            this.useWarDraw();
       
            //Boolean to retrieve the result
            Boolean d = draw.getResult();
            if (d != true) {
                
                } else {
                //Pane used to show there was a card draw
                   JOptionPane.showMessageDialog( null, "There was a draw!!!,  WAR", "War", JOptionPane.OK_OPTION);
                   this.doDraw();
                   this.mySwitch();
                   this.useWarDraw();
                   didDraw = draw.getDraw();
            }
            //Determines the number and matching card
            switch (rNum) {

                case 0:
                    textPane.append("\n" + ranks[0] + " of " + suits[0]);
                    ci.getAceHearts();
                    

                    break;
                case 1:
                    textPane.append("\n" + ranks[0] + " of " + suits[1]);
                    ci.getAceSpades();

                    break;

                case 2:
                    textPane.append("\n" + ranks[0] + " of " + suits[2]);
                    ci.getAceDia();

                    break;
                case 3:
                    textPane.append("\n" + ranks[0] + " of " + suits[3]);
                    ci.getAceClubs();

                    break;
                case 4:
                    textPane.append("\n" + ranks[1] + " of " + suits[0]);
                    ci.getTwoHearts();

                    break;
                case 5:
                    textPane.append("\n" + ranks[1] + " of " + suits[1]);
                    ci.getTwoSpades();

                    break;
                case 6:
                    textPane.append("\n" + ranks[1] + " of " + suits[2]);
                    ci.getTwoDia();

                    break;
                case 7:
                    textPane.append("\n" + ranks[1] + " of " + suits[3]);
                    ci.getTwoClubs();

                    break;
                case 8:
                    textPane.append("\n" + ranks[2] + " of " + suits[0]);
                    ci.getThreeHearts();

                    break;
                case 9:
                    textPane.append("\n" + ranks[2] + " of " + suits[1]);
                    ci.getThreeSpades();

                    break;
                case 10:
                    textPane.append("\n" + ranks[2] + " of " + suits[2]);
                    ci.getThreeDia();

                    break;
                case 11:
                    textPane.append("\n" + ranks[2] + " of " + suits[3]);
                    ci.getThreeClubs();

                    break;
                case 12:
                    textPane.append("\n" + ranks[3] + " of " + suits[0]);
                    ci.getFourHearts();

                    break;
                case 13:
                    textPane.append("\n" + ranks[3] + " of " + suits[1]);
                    ci.getFourSpades();

                    break;

                case 14:
                    textPane.append("\n" + ranks[3] + " of " + suits[2]);
                    ci.getFourDia();

                    break;
                case 15:
                    textPane.append("\n" + ranks[3] + " of " + suits[3]);
                    ci.getFourClubs();

                    break;
                case 16:
                    textPane.append("\n" + ranks[4] + " of " + suits[0]);
                    ci.getFiveHearts();

                    break;
                case 17:
                    textPane.append("\n" + ranks[4] + " of " + suits[1]);
                    ci.getFiveSpades();

                    break;
                case 18:
                    textPane.append("\n" + ranks[4] + " of " + suits[2]);
                    ci.getFiveDia();

                    break;
                case 19:
                    textPane.append("\n" + ranks[4] + " of " + suits[3]);
                    ci.getFiveClubs();

                    break;
                case 20:
                    textPane.append("\n" + ranks[5] + " of " + suits[0]);
                    ci.getSixHearts();

                    break;
                case 21:
                    textPane.append("\n" + ranks[5] + " of " + suits[1]);
                    ci.getSixSpades();

                    break;
                case 22:
                    textPane.append("\n" + ranks[5] + " of " + suits[2]);
                    ci.getSixDia();

                    break;
                case 23:
                    textPane.append("\n" + ranks[5] + " of " + suits[3]);
                    ci.getSixClubs();

                    break;
                case 24:
                    textPane.append("\n" + ranks[6] + " of " + suits[0]);
                    ci.getSevenHearts();

                    break;
                case 25:
                    textPane.append("\n" + ranks[6] + " of " + suits[1]);
                    ci.getSevenSpades();

                    break;
                case 26:
                    textPane.append("\n" + ranks[6] + "of" + suits[2]);
                    ci.getSevenDia();

                    break;
                case 27:
                    textPane.append("\n" + ranks[6] + " of " + suits[3]);
                    ci.getSevenClubs();

                    break;
                case 28:
                    textPane.append("\n" + ranks[7] + " of " + suits[0]);
                    ci.getEightHearts();

                    break;
                case 29:
                    textPane.append("\n" + ranks[7] + " of " + suits[1]);
                    ci.getEightSpades();

                    break;
                case 30:
                    textPane.append("\n" + ranks[7] + " of " + suits[2]);
                    ci.getEightDia();

                    break;
                case 31:
                    textPane.append("\n" + ranks[7] + " of " + suits[3]);
                    ci.getEightClubs();

                    break;
                case 32:
                    textPane.append("\n" + ranks[8] + " of " + suits[0]);
                    ci.getNineHearts();

                    break;
                case 33:
                    textPane.append("\n" + ranks[8] + "of" + suits[1]);
                    ci.getNineSpades();

                    break;
                case 34:
                    textPane.append("\n" + ranks[8] + " of " + suits[2]);
                    ci.getNineDia();

                    break;
                case 35:
                    textPane.append("\n" + ranks[8] + " of " + suits[3]);
                    ci.getNineClubs();

                    break;
                case 36:
                    textPane.append("\n" + ranks[9] + " of " + suits[0]);
                    ci.getTenHearts();

                    break;
                case 37:
                    textPane.append("\n" + ranks[9] + " of " + suits[1]);
                    ci.getTenSpades();

                    break;
                case 38:
                    textPane.append("\n" + ranks[9] + " of " + suits[2]);
                    ci.getTenDia();

                    break;
                case 39:
                    textPane.append("\n" + ranks[9] + " of " + suits[3]);
                    ci.getTenClubs();

                    break;
                case 40:
                    textPane.append("\n" + ranks[10] + " of " + suits[0]);
                    ci.getJackHearts();

                    break;
                case 41:
                    textPane.append("\n" + ranks[10] + " of " + suits[1]);
                    ci.getJackSpades();

                    break;
                case 42:
                    textPane.append("\n" + ranks[10] + " of " + suits[2]);
                    ci.getJackDia();

                    break;
                case 43:
                    textPane.append("\n" + ranks[10] + " of " + suits[3]);
                    ci.getJackClubs();

                    break;
                case 44:
                    textPane.append("\n" + ranks[11] + " of " + suits[0]);
                    ci.getQueenHearts();

                    break;
                case 45:
                    textPane.append("\n" + ranks[11] + " of " + suits[1]);
                    ci.getQueenSpades();

                    break;
                case 46:
                    textPane.append("\n" + ranks[11] + " of " + suits[2]);
                    ci.getQueenDia();

                    break;
                case 47:
                    textPane.append("\n" + ranks[11] + " of " + suits[3]);
                    ci.getQueenClubs();

                    break;
                case 48:
                    textPane.append("\n" + ranks[12] + " of " + suits[0]);
                    ci.getKingHearts();

                    break;
                case 49:
                    textPane.append("\n" + ranks[12] + " of " + suits[1]);
                    ci.getKingSpades();

                    break;
                case 50:
                    textPane.append("\n" + ranks[12] + " of " + suits[2]);
                    ci.getKingDia();

                    break;
                case 51:
                    textPane.append("\n" + ranks[12] + " of " + suits[3]);
                    ci.getKingClubs();

                    break;
                default:
                    System.out.print(rW);
                    break;
            }
            }
         if(twoTurns <= 0 || oneTurns <= 0){
                   draw.getOne();
                   int[] p = new int[2];
                   p = draw.getOne();
                if(p[0] > p[1]){
                    JOptionPane.showMessageDialog(null, "PlayerOne Wins!!", "The End is Here", JOptionPane.ERROR_MESSAGE);
                }else if(p[0] < p[1]){
                    JOptionPane.showMessageDialog(null, "PlayerTwo Wins!!", "The End is Here", JOptionPane.ERROR_MESSAGE);
                }
                JOptionPane.showMessageDialog(null, "Game Over", "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            
        } else if (o == playerOneButton) {
            playerTwoButton.setVisible(true);
            playerOneButton.setVisible(false);
            
            
            try {
                rW = rankWar.nextInt((randomRank) + 1);

                System.out.print(randomRank);
            } catch (IllegalArgumentException exc) {
                System.err.println(exc);
                JOptionPane.showMessageDialog(null, "Game Over", "Error", JOptionPane.ERROR_MESSAGE);

            }
         //Uses the mySwitch method to compare value
         mySwitch();
           
            if(oneTurns == 1 || twoTurns == 1){
                JOptionPane.showMessageDialog(null, "PlayerOne has one more cards!! OR PlayerTwo has one more cards", "The End is Near", JOptionPane.ERROR_MESSAGE);
    
    }
        }
    }
}