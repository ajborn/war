package bsawar;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/


import java.awt.BorderLayout;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Austin
 */
public class CardImages extends JLabel{
     JLabel temp = new JLabel();
    String[] paths = {"1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png", "9.png", "10.png", "11.png", "12.png", "13.png", "14.png"
    , "15.png", "16.png", "17.png", "18.png", "19.png", "20.png", "21.png", "22.png", "23.png", "24.png", "25.png", "26.png", "27.png", "28.png",
    "29.png", "30.png", "31.png", "32.png", "33.png", "34.png", "35.png", "36.png", "37.png", "38.png", "39.png", "40.png", "41.png", "42.png", "43.png",
    "44.png", "45.png", "46.png", "47.png", "48.png", "49.png", "50.png", "51.png", "52.png"};
    
    public void getAceClubs(){
        
         ImageIcon aceClubs = ourImage(paths[0]);
         this.setIcon(aceClubs);

    }
    public JLabel getTwoClubs(){
        
         ImageIcon twoClubs = ourImage(paths[48]);
         temp = new JLabel(twoClubs);
         this.setIcon(twoClubs);
         return temp;
    }
    public void getThreeClubs(){
        
         ImageIcon threeClubs = ourImage(paths[44]);
         this.setIcon(threeClubs);

    }
    public void getFourClubs(){
        
         ImageIcon fourClubs = ourImage(paths[40]);
         this.setIcon(fourClubs);

    }
    public void getFiveClubs(){
        
         ImageIcon fiveClubs = ourImage(paths[36]);
         this.setIcon(fiveClubs);

    }
    public void getSixClubs(){
        
         ImageIcon sixClubs = ourImage(paths[32]);
         this.setIcon(sixClubs);

    }
    public void getSevenClubs(){
        
         ImageIcon sevenClubs = ourImage(paths[28]);
         this.setIcon(sevenClubs);

    }
    public void getEightClubs(){
        
         ImageIcon eightClubs = ourImage(paths[24]);
         this.setIcon(eightClubs);

    }
    public void getNineClubs(){
        
         ImageIcon nineClubs = ourImage(paths[20]);
         this.setIcon(nineClubs);

    }
    public void getTenClubs(){
        
         ImageIcon tenClubs = ourImage(paths[16]);
         this.setIcon(tenClubs);

    }
    public void getJackClubs(){
        System.out.println("jack clubs");
         ImageIcon jackClubs = ourImage(paths[12]);
         this.setIcon(jackClubs);

    }
    public void getQueenClubs(){
        
         ImageIcon queenClubs = ourImage(paths[8]);
         this.setIcon(queenClubs);

    }
    public void getKingClubs(){
        
         ImageIcon kingClubs = ourImage(paths[4]);
         this.setIcon(kingClubs);

    }
    public void getAceSpades(){
        
         ImageIcon aceSpades = ourImage(paths[1]);
         this.setIcon(aceSpades);

    }
    public void getTwoSpades(){
        
         ImageIcon twoSpades = ourImage(paths[49]);
         this.setIcon(twoSpades);

    }
    public void getThreeSpades(){
        
         ImageIcon threeSpades = ourImage(paths[45]);
         this.setIcon(threeSpades);

    }
    public void getFourSpades(){
        
         ImageIcon fourSpades = ourImage(paths[41]);
         this.setIcon(fourSpades);

    }
    public void getFiveSpades(){
        
         ImageIcon fiveSpades = ourImage(paths[37]);
         this.setIcon(fiveSpades);

    }
    public void getSixSpades(){
        
         ImageIcon sixSpades = ourImage(paths[33]);
         this.setIcon(sixSpades);

    }
    public void getSevenSpades(){
        
         ImageIcon sevenSpades = ourImage(paths[29]);
         this.setIcon(sevenSpades);

    }
    public void getEightSpades(){
        
         ImageIcon eightSpades = ourImage(paths[25]);
         this.setIcon(eightSpades);

    }
    public void getNineSpades(){
        
         ImageIcon nineSpades = ourImage(paths[21]);
         this.setIcon(nineSpades);

    }
    public void getTenSpades(){
        
         ImageIcon tenSpades = ourImage(paths[17]);
         this.setIcon(tenSpades);

    }
    public void getJackSpades(){
        
         ImageIcon jackSpades = ourImage(paths[13]);
         this.setIcon(jackSpades);

    }
    public void getQueenSpades(){
        
         ImageIcon queenSpades = ourImage(paths[9]);
         this.setIcon(queenSpades);

    }
    public void getKingSpades(){
        
         ImageIcon kingSpades = ourImage(paths[5]);
         this.setIcon(kingSpades);

    }
    public void getAceHearts(){
        
         ImageIcon aceHearts = ourImage(paths[2]);
         this.setIcon(aceHearts);

    }
    public void getTwoHearts(){
        
         ImageIcon twoHearts = ourImage(paths[50]);
         this.setIcon(twoHearts);

    }
    public void getThreeHearts(){
        
         ImageIcon threeHearts = ourImage(paths[46]);
         this.setIcon(threeHearts);

    }
    public void getFourHearts(){
        
         ImageIcon fourHearts = ourImage(paths[42]);
         this.setIcon(fourHearts);

    }
    public void getFiveHearts(){
        
         ImageIcon fiveHearts = ourImage(paths[38]);
         this.setIcon(fiveHearts);

    }
    public void getSixHearts(){
        
         ImageIcon sixHearts = ourImage(paths[34]);
         this.setIcon(sixHearts);

    }
    public void getSevenHearts(){
        
         ImageIcon sevenHearts = ourImage(paths[30]);
         this.setIcon(sevenHearts);

    }
    public void getEightHearts(){
        
         ImageIcon eightHearts = ourImage(paths[26]);
         this.setIcon(eightHearts);

    }
    public void getNineHearts(){
        
         ImageIcon nineHearts = ourImage(paths[22]);
         this.setIcon(nineHearts);

    }
    public void getTenHearts(){
        
         ImageIcon tenHearts = ourImage(paths[18]);
         this.setIcon(tenHearts);

    }
    public void getJackHearts(){
        
         ImageIcon jackHearts = ourImage(paths[14]);
         this.setIcon(jackHearts);

    }
    public void getQueenHearts(){
        
         ImageIcon queenHearts = ourImage(paths[10]);
         this.setIcon(queenHearts);

    }
    public void getKingHearts(){
        
         ImageIcon kingHearts = ourImage(paths[6]);
         this.setIcon(kingHearts);

    }
    public void getAceDia(){
        
         ImageIcon aceDia = ourImage(paths[3]);
         this.setIcon(aceDia);

    }
    public void getTwoDia(){
        
         ImageIcon twoDia = ourImage(paths[51]);
         this.setIcon(twoDia);

    }
    public void getThreeDia(){
        
         ImageIcon threeDia = ourImage(paths[47]);
         this.setIcon(threeDia);

    }
    public void getFourDia(){
        
         ImageIcon fourDia = ourImage(paths[43]);
         this.setIcon(fourDia);

    }
    public void getFiveDia(){
        
         ImageIcon fiveDia = ourImage(paths[39]);
         this.setIcon(fiveDia);

    }
    public void getSixDia(){
        
         ImageIcon sixDia = ourImage(paths[35]);
         this.setIcon(sixDia);

    }
    public void getSevenDia(){
        
         ImageIcon sevenDia = ourImage(paths[31]);
         this.setIcon(sevenDia);

    }
    public void getEightDia(){
        
         ImageIcon eightDia = ourImage(paths[27]);
         this.setIcon(eightDia);

    }
    public void getNineDia(){
        
         ImageIcon nineDia = ourImage(paths[23]);
         this.setIcon(nineDia);

    }
    public void getTenDia(){
        
         ImageIcon tenDia = ourImage(paths[19]);
         this.setIcon(tenDia);

    }
    public void getJackDia(){
        
         ImageIcon jackDia = ourImage(paths[15]);
         this.setIcon(jackDia);

    }
    public void getQueenDia(){
        System.out.println("queen dia");
         ImageIcon queenDia = ourImage(paths[15]);
         this.setIcon(queenDia);

    }
    public void getKingDia(){
        
         ImageIcon kingDia = ourImage(paths[7]);
         this.setIcon(kingDia);

    }
    
 //A method that uses the Image Icon java class to create a URL
    //and retrieve the relative path of the file
    //If the file does not exist the console will return an error statement
    private ImageIcon ourImage(String thePath) {
        URL theURL = getClass().getResource(thePath);
        if (theURL != null) {
            return new ImageIcon(theURL);
        } else {
            System.err.println("Couldn't find file: " + thePath);
            return null;
        }
    }    
}

